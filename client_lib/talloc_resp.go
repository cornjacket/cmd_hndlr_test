package client_lib 

import (

	"fmt"
	"encoding/json"
	"bytes"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

type TallocRespCmdHndlrService struct {
	Hostname  string
	Port      string
	Path      string
	Enabled   bool
}

// TODO: Validate Hostname and Port within reason
func (s *TallocRespCmdHndlrService) Open(Hostname, Port, Path string) error {
	s.Hostname = Hostname
	s.Port = Port
	s.Path = Path
	s.Enabled = true
	return s.Ping()	
}

func (s *TallocRespCmdHndlrService) URL() string {
	if s.Enabled {
		return "http://" + s.Hostname + ":" + s.Port + s.Path 
	}
	return ""
}

// TODO: This needs to be done...
// Used to determine if the dependent service is currently up.
// Ping should make multiple attempts to see if the external service is up before giving up and returning an error.
func (s *TallocRespCmdHndlrService) Ping() (error) {
	// check if enabled
	// if so, then make a GET call to /ping of the URL()
	// if there is an error, then repeatedly call for X times with linear backoff until success
/* Keep and add to services...
        // ping loop
        var pingErr error
        for i := 0; i < 15; i++ {
                fmt.Printf("Pinging %s\n", DbHost)
                pingErr = server.DB.DB().Ping()
                if pingErr != nil {
                        fmt.Println(pingErr)
                } else {
                        fmt.Println("Ping replied.")
                        break;
                }
                time.Sleep(time.Duration(3) * time.Second)
        }
        if pingErr != nil {
                log.Fatal("This is the error:", err)
        }
*/

	return nil
}

func (s *TallocRespCmdHndlrService) SendTallocResp(tallocresp tallocrespproc.TallocResp) (error) {

	var err error
	if s.Enabled {
		//fmt.Printf("TallocRespCmdHndlrService: Sending tallocresp to %s\n", s.URL())
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(tallocresp)
		//err = httpx.Post(s.URL(), b)
		err = Post(s.URL(), b)
	} else {
		fmt.Printf("TallocRespCmdHndlrService is disabled. Dropping packet.\n")
	}
	return err 

}
