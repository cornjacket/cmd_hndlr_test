package client_lib

import (

	"errors"
	"net/http"
	"bytes"
	"io/ioutil"
)

// This function can be extracted and included in the uService clientlib for a Post function
// post will return error on non 200 or 201 status code
func Post(url string, b *bytes.Buffer) (error) {
	var res *http.Response
	var err error
	res, err = http.Post(url, "application/json; charset=utf-8", b) // Should I defer close of the res.Body
	//fmt.Printf("statusCreated: %d\n", http.StatusCreated)
	//fmt.Printf("statusOK: %d\n", http.StatusOK)
	if err == nil {
		//fmt.Printf("http_post nil: statusCode: %d\n", res.StatusCode)
		if res.StatusCode != http.StatusOK && res.StatusCode != http.StatusCreated {
			body, _ := ioutil.ReadAll(res.Body)
			err = errors.New(string(body))
		}
	}
	//fmt.Printf("http_post: statusCode: %d\n", res.StatusCode)
	//if err == nil {
	//	fmt.Printf("http_post error is nil\n")
		//io.Copy(os.Stdout, res.Body)
		//fmt.Println()
	//}
	return err
}
