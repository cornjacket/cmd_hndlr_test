module bitbucket.org/cornjacket/cmd_hndlr

go 1.13

require (
	bitbucket.org/cornjacket/airtrafficcontrol/client v0.1.3
	bitbucket.org/cornjacket/event_hndlr v0.1.1
	bitbucket.org/cornjacket/iot v0.1.7
	github.com/go-openapi/runtime v0.19.21
	github.com/gorilla/context v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
)
