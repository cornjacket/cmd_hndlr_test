package main

import (
	"bitbucket.org/cornjacket/cmd_hndlr/app"
)

func main() {

	cmdApp := app.NewAppService()	
	cmdApp.Init()
	cmdApp.Run()

}

