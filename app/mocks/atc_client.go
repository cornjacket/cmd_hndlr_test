package mocks

import (
	"log"
	"bitbucket.org/cornjacket/iot/message"
)

// Should I rename this clientMock
type AirTrafficControlServiceMock struct {
}

var AtcLastPacket	message.UpPacket

func (s *AirTrafficControlServiceMock) Open(Hostname, Port, PacketPath, TallocReqPath string) error {
	AtcLastPacket = message.UpPacket{}
	return nil
}

func (s *AirTrafficControlServiceMock) SendPacket(packet message.UpPacket) (error) {
	log.Println("AtcClientMock.SendPacket() invoked.")
	AtcLastPacket = packet
	return nil 
}
