package app

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/cornjacket/cmd_hndlr/app/controllers"
	"bitbucket.org/cornjacket/cmd_hndlr/app/utils/env"
	client "bitbucket.org/cornjacket/event_hndlr/client_lib"
)

type AppService struct {
	Env     AppEnv
	Context *controllers.AppContext
}

type AppEnv struct {
	PortNum       string
	KafkaHostname string
	KafkaPort     string
}

func NewAppService() AppService {
	a := AppService{}
	a.Context = controllers.NewAppContext()
	return a
}

func (a *AppService) InitEnv() {

	a.Env.PortNum = env.GetVar("CMD_PORT_NUM", "8080", false)
	a.Env.KafkaHostname = os.Getenv("KAFKA_HOSTNAME")
	a.Env.KafkaPort = env.GetVar("KAFKA_PORT", "9092", false)

}

func (a *AppService) SetupExternalServices() {

	transportType := client.HttpPost
	if a.Env.KafkaHostname != "" {
		transportType = client.Kafka
	}

	eventHndlrHost := env.GetVar("EVENT_HNDLR_HOSTNAME", "localhost", false)
	eventHndlrPort := env.GetVar("EVENT_HNDLR_PORT", "8081", false)
	packetEventHndlrPath := env.GetVar("PACKET_EVENT_HNDLR_PATH", "/packet", false)
	tallocRespEventHndlrPath := env.GetVar("TALLOC_RESP_EVENT_HNDLR_PATH", "/tallocResp.4.3.6", false)

	airTrafficControlHost := env.GetVar("ATC_HOSTNAME", "localhost", false)
	airTrafficControlPort := env.GetVar("ATC_PORT", "8083", false)
	airTrafficControlPath := env.GetVar("ATC_PACKET_PACKET_PATH", "/packet", false)
	//airTrafficControlTallocRespPath := env.GetVar("ATC_PACKET_TALLOCRESP_PATH", "/tallocresp", false)

	// not sure if I am goin to use discovery with the CQRS pattern...
	//discoveryHostname := env.GetVar("DISCOVERY_HOSTNAME", "", false)
	//discoveryPort := env.GetVar("DISCOVERY_PORT", "8001", false)

	// TODO: This is where Discovery should be checked and action taken...which can overide other env variables

	// TODO: This is where a get call should
	eventHndlrConfig := client.EventHndlrServiceConfig{
		TransportType:   transportType,
		KafkaHostname:   a.Env.KafkaHostname,
		KafkaPort:       a.Env.KafkaPort,
		PacketTopic:     "cmd_event_packet",
		TallocRespTopic: "cmd_event_tallocresp",
		Hostname:        eventHndlrHost,
		Port:            eventHndlrPort,
		PacketPath:      packetEventHndlrPath,
		TallocRespPath:  tallocRespEventHndlrPath,
	}
	//if err = a.Context.EventHndlrService.Open(eventHndlrHost, eventHndlrPort, packetEventHndlrPath, tallocRespEventHndlrPath); err != nil {
	if err := a.Context.EventHndlrService.Open(eventHndlrConfig); err != nil {
		log.Fatal("EventHndrlService.Open() error: ", err)
	}
	TallocRespPathNotUsed := ""
	// todo(DRT) - I should probably break up the atc client into 2 clients, packet_client and tallocresp_client
	if err := a.Context.AirTrafficControlService.Open(airTrafficControlHost, airTrafficControlPort, airTrafficControlPath, TallocRespPathNotUsed); err != nil {
		log.Fatal("AirTrafficControlService.Open() error: ", err)
	}

}

func (a *AppService) Init() {
	a.InitEnv()
	a.SetupExternalServices()
}

func (a *AppService) Run() {

	checkIfProjectNeedsBuild() // will this work anymore now that build file will be in subfolder?
	a.Context.Run(a.Env.PortNum)

}

func checkIfProjectNeedsBuild() {
	if _, err := os.Stat("./build.json"); err == nil {
		fmt.Println("Project config has changed but build was not invoked. Run \"guild Build\"")
		os.Exit(1)
	}
}
