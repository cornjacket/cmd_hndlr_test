package controllers

import (
	// Standard library packets
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
	"github.com/julienschmidt/httprouter"
)

// TODO: This should live within the appContext
var descr ApplicationDescriptor

// A tallocrespproc.TallocResp is returned in the response
// swagger:response tallocrespResponse
type tallocrespResponseWrapper struct {
	// tallocresp send to Command Handler
	// in: body
	Body tallocrespproc.TallocResp
}

// swagger:route POST /tallocresp tallocresp sendTallocresp
// Sends a Tallocresp to the EventHandler via the Command Handler.
// Returns Tallocresp to client
// responses:
//   201: tallocrespResponse

// This function returns the entry point for the tallocresp processing worker pool upon reception of
// a post to /tallocresp. The default behavior is for the tallocresp post worker to immediately queue the tallocresp
// followed by replying to the client with the posted tallocresp.
func (app *AppContext) NewTallocRespPoolEntryFunc(numWorkers int, init ApplicationDescriptor) func(http.ResponseWriter, *http.Request, httprouter.Params) {

	descr = init
	return tallocrespproc.NewResponseHandlerFunc(numWorkers, app.tallocRespHandler)

}

func (app *AppContext) tallocRespHandler(wID int, tresp tallocrespproc.TallocResp) bool {

	// TODO: validate the tresp prior to forwarding to the event handler, i.e. classId, NodeEui format are valid, ...
	err := app.EventHndlrService.SendTallocResp(tresp)
	tallocrespproc.DisplayPassFailMessageWithLatency("send talloc resp to App Event Handler", err, wID, &tresp, true)
	return true
}
