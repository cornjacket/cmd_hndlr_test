package controllers

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
)

func (app *AppContext) initializeRoutes() {

	// Home Route
	app.Router.GET("/", wrapHandlerFunc(app.Home))

	// Packet Route
	//app.Router.POST("/AppNum4AppApi3SysApi6", app.NewParserPoolEntryFunc("4.3.6", 100))
	app.Router.POST("/packet", app.NewParserPoolEntryFunc("4.3.6", 100))

	// TallocResp Router
	// app-descriptor should be part of AppContext
	app.Router.POST("/tallocresp", app.NewTallocRespPoolEntryFunc(100, ApplicationDescriptor{appNum: 4, appApi: 3, sysApi: 6}))

	// Swagger API /docs path
	opts := middleware.RedocOpts{SpecURL: "/swagger.yaml"}
	sh := middleware.Redoc(opts, nil)
	app.Router.GET("/docs", wrapHandlerFunc(sh.ServeHTTP))
	app.Router.GET("/swagger.yaml", wrapHandlerFunc(http.FileServer(http.Dir("./app")).ServeHTTP))

}

// Wrapper function to make http handlers (i.e. goriall/mux) work with httprouter (i.e. julienschmidt)
// gorilla/context may be useful in the future if other http handlers (besides home) need to be converted to httprouter.
// source: https://www.nicolasmerouze.com/guide-routers-golang
func wrapHandlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		context.Set(r, "params", ps)
		h.ServeHTTP(w, r)
	}
}
