package controllers

import (
	"net/http"

	"bitbucket.org/cornjacket/cmd_hndlr/app/responses"
)

func (app *AppContext) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Service Up")

}
