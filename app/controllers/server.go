// Package classification Cmd API.
//
// Documentation for Cmd API
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http
//     Host: localhost
//     BasePath: /
//     Version: 0.1.0
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: David Taylor<taylor.david.ray@gmail.com> http://john.doe.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
// swagger:meta
package controllers

import (
	"fmt"
	"log"
	"net/http"

	atc_client "bitbucket.org/cornjacket/airtrafficcontrol/client"
	event_client "bitbucket.org/cornjacket/event_hndlr/client_lib"
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
	"github.com/julienschmidt/httprouter"
	//"github.com/rs/cors"
)

type EventHandlrPostInterface interface {
	Open(event_client.EventHndlrServiceConfig) error
	//Open(Hostname, Port, PacketPath, TallocRespPath string) error
	SendPacket(packet message.UpPacket) error
	SendTallocResp(tallocresp tallocrespproc.TallocResp) error
}

type AirTrafficControlPostInterface interface {
	Open(Hostname, Port, PacketPath, TallocReqPath string) error
	SendPacket(packet message.UpPacket) error
}

type AppContext struct {
	Router                   *httprouter.Router
	EventHndlrService        EventHandlrPostInterface
	AirTrafficControlService AirTrafficControlPostInterface
}

func NewAppContext() *AppContext {
	context := AppContext{}
	context.EventHndlrService = &event_client.EventHndlrService{}             // implements EventHandlrPostInterface
	context.AirTrafficControlService = &atc_client.AirTrafficControlService{} // implements AirTrafficControlPostInterface
	return &context
}

func (app *AppContext) Run(addr string) {
	// Instantiate a new router
	app.Router = httprouter.New()

	app.initializeRoutes()

	// Add CORS support (Cross Origin Resource Sharing)
	//handler := cors.Default().Handler(app.Router)
	//err = http.ListenAndServe(":"+addr, handler)
	//if err != nil {
	//	logger.Fatal(err)
	//}

	//go component.Run(addr) // quick testing for now
	fmt.Printf("Listening to port %s\n", addr)
	log.Fatal(http.ListenAndServe(":"+addr, app.Router))
	//log.Fatal(http.ListenAndServe(":"+addr, handler))
}
